#!/usr/bin/env ../libexec/ohmyclj
"DEPS='clj-time=RELEASE'"

(ns user
  (:require
   [clj-time.core :as t]))

(defn greet [msg]
  (str "Hello, " msg "!"))

(defn -main [& args]
  (prn :args args))


(ns test
  (:require
   [clojure.test :refer [deftest is testing]]
   [user]))

(deftest greet-test
  (is (= "Hello, World!" (user/greet "World"))))
