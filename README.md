# ohmyclj

dev/test/run standalone Clojure scripts with ease

[![pipeline status](https://gitlab.com/eval/ohmyclj/badges/master/pipeline.svg)](https://gitlab.com/eval/ohmyclj/commits/master)
[![discuss at Clojurians-Zulip](https://img.shields.io/badge/clojurians%20zulip-ohmyclj-brightgreen.svg)](https://clojurians.zulipchat.com/#narrow/stream/192170-ohmyclj)

## install

Requirements:
- [Clojure](https://www.clojure.org/guides/getting_started#_clojure_installer_and_cli_tools)

Put `ohmyclj` on your path:
``` shell
$ curl https://gitlab.com/eval/ohmyclj/raw/0.2/libexec/ohmyclj > ~/bin/ohmyclj
$ chmod +x ~/bin/ohmyclj
```

## quickstart

```shell
# generate it
$ ohmyclj new > hello_world.clj
# add deps, fix the tests etc
$ $EDITOR hello_world.clj
# try it
$ ohmyclj repl hello_world.clj
# test it
$ ohmyclj test hello_world.clj
# run it
$ ohmyclj run hello_world.clj 'Hello!'
# or
$ chmod +x hello_world.clj
$ ./hello_world.clj
```

## usage

### run a script

Given the following (executable) file `hello_world.clj`:

``` clojure
#!/usr/bin/env ohmyclj
"DEPS='clj-time=0.15.0'"

(ns user
  (:require [clj-time.core :as t]))

(defn greet [name]
  (str "Hello, " name "!"))

(defn -main [& args]
  (println (-> args first greet))
  (println (str "Time is now " (t/now))))
```

...run it with:

``` shell
$ ./hello_world.clj World
Hello, World!
Time is now 2019-03-10T14:56:13.814Z
```

Let's take a closer look at `hello_world.clj`:
1. dependencies  
    Dependencies for a script are added to "DEPS=...".
    Multiple dependencies are separated by `;`, e.g. `"DEPS='clj-time=0.15.0;some-other=1.2.3'"`

1. user/-main  
    This is the starting point of the script. It's invoked with the command line arguments.

### run tests

Tests should be added to the test-namespace.

So given the following addition to `hello_world.clj`:

``` clojure
... user-namespace with greet-function

(ns test
  (:require [clojure.test :refer [deftest is testing]]
            [user]))

(deftest greet-test
  (is (= "Hello, Clojure!" (user/greet "Clojure"))))
```

...the tests can be run like so:

``` shell
$ ohmyclj test hello_world.clj
Testing test

Ran 1 tests containing 1 assertions.
0 failures, 0 errors.
```

### repl

Fire up a repl with [Rebel readline editor](https://github.com/bhauman/rebel-readline):
```shell
$ ohmyclj repl hello_world.clj
#'user/reload
#'user/run-tests
[Rebel readline] Type :repl/help for online help info
user=> 
```

Two helper functions are at your disposal:
- reload - to get the latest changes to the script in the repl
- run-tests - to run the tests

### generate script

```shell
$ ohmyclj new > hello_world.clj
```

## license

See [LICENSE](LICENSE).
