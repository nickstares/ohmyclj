run_ohmyclj() {
  script="$1"
  shift
  run "${BATS_TEST_DIRNAME}/../libexec/ohmyclj" run "${BATS_TEST_DIRNAME}/e2e/${script}" "$@"
}

@test "e2e tools-deps version" {
  echo "# tools-deps-version $(grep Version `which clojure`)" >&3
}

@test "sample.clj" {
  run_ohmyclj "sample.clj"

  [ $status -eq 0 ]
  [ "$output" == "Hello World" ]
}

@test "with-main.clj: no args" {
  run_ohmyclj 'with-main.clj'

  [ $status -eq 0 ]
  [ "$output" == ":args nil" ]
}

@test "with-main.clj: with args" {
  run_ohmyclj 'with-main.clj' 1 2 'and 3'
  #echo $output
  [ $status -eq 0 ]
  [ "$output" == ":args (\"1\" \"2\" \"and 3\")" ]
}

@test "with-deps.clj" {
  run_ohmyclj 'with-deps.clj'
  

  [ $status -eq 0 ]

  #echo $output
  [[ "$output" =~ "1986-10-14T00:00:00.000Z" ]]
}
