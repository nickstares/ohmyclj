#!/usr/bin/env bats

setup() {
  . "$BATS_TEST_DIRNAME/../libexec/ohmyclj"
}

teardown() {
  rm -f "${BATS_TEST_DIRNAME}/sample.clj"
}

@test "is_git_dep: https-url" {
  run is_git_dep "https://gitlab.com/user/some-repos" "12345678"

  [ "$status" -eq 0 ]
}

@test "is_git_dep: git@-url" {
  run is_git_dep "git@github.com/user/some-repos" "12345678"

  [ "$status" -eq 0 ]
}

@test "is_git_dep: non-example" {
  run is_git_dep "not-a-git-dep" "123"

  [ "$status" -ne 0 ]
}

# is_local_dep
#

@test "is_local_dep: absolute path" {
  run is_local_dep "/some/path/to/folder-with-deps-edn"

  [ "$status" -eq 0 ]
}

@test "is_local_dep: relative path" {
  run is_local_dep "../some/path/to/folder-with-deps-edn"

  [ "$status" -eq 0 ]
}

@test "is_local_dep: not a path" {
  run is_local_dep "not a path"

  [ "$status" -ne 0 ]
}

@test "is_local_dep: home-folder" {
  run is_local_dep "~/path/to/folder"

  [ "$status" -ne 0 ]
}


# expand_dep
#

@test "expand_dep: simple dep" {
  run expand_dep "org.clojure/clojure" "1.10"

  [ "$output" == "org.clojure/clojure {:mvn/version \"1.10\"}" ]
}

@test "expand_dep: git-dep" {
  run expand_dep "https://github.com/healthfinch/depstar.git" "4aa7b35189693feebc7d7e4a180b8af0326c9164"

  [ "$output" == "github.com/healthfinch/depstar.git {:git/url \"https://github.com/healthfinch/depstar.git\" :sha \"4aa7b35189693feebc7d7e4a180b8af0326c9164\"}" ]
}

@test "expand_dep: local" {
  run expand_dep "my-project" "/path/to/said-project"

  [ "$output" == "my-project {:local/root \"/path/to/said-project\"}" ]
}

@test "extract_var_from_script: no deps" {
  echo "(println \"some clj\")" > "${BATS_TEST_DIRNAME}/sample.clj"
  extract_var_from_script "DEPS" "${BATS_TEST_DIRNAME}/sample.clj"

  # $DEPS is unset
  [ -z "${DEPS+x}"]
}

@test "extract_var_from_script" {
  echo "\"DEPS='clj-time=RELEASE;some-other=1.2.3'\"" > "${BATS_TEST_DIRNAME}/sample.clj"
  extract_var_from_script DEPS "${BATS_TEST_DIRNAME}/sample.clj"

  [ "$DEPS" == 'clj-time=RELEASE;some-other=1.2.3' ]
}

# expand_deps
#

@test "expand_deps: all" {
  run expand_deps "clj-time=RELEASE;some-other=1.2.3"

  [[ $output =~ "some-other {:mvn/version \"1.2.3\"} clj-time {:mvn/version \"RELEASE\"}" ]]
}
